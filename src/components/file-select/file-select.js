import Vue from 'vue';
import Component from 'vue-class-component';
import Axios from 'axios';

@Component({

})
export default class FileSelect extends Vue {
  handleFileChange(e) {
    // Whenever the file changes, emit the 'input' event with the file data.
    this.$emit('input', e.target.files[0])
    console.log('na...', e.target.files[0])
    // fileLoaded = e.target.files.length > 0;
  }
  created() {
    console.log('shit\'s createdd');
  }
  sendFileToServer() {


    // Create/open database
    var request = createDatabase();
    startServiceWorker();

  }

  startServiceWorker() {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('./services/process-file-worker.js')
        .then(function (response) {

          // Service worker registration done
          console.log('Registration Successful', response);
        }, function (error) {
          // Service worker registration failed
          console.log('Registration Failed', error);
        })
    }
  }

  createDatabase() {
    window.indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.OIndexedDB || window.msIndexedDB,
      IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.OIDBTransaction || window.msIDBTransaction,
      dbVersion = 1;
    var request = indexedDB.open("mapperFiles", dbVersion);
    request.onsuccess = function (event) {
      console.log("Success creating/accessing IndexedDB database");
      db = request.result;

      db.onerror = function (event) {
        console.log("Error creating/accessing IndexedDB database");
      };

      // Interim solution for Google Chrome to create an objectStore. Will be deprecated
      if (db.setVersion) {
        if (db.version != dbVersion) {
          var setVersion = db.setVersion(dbVersion);
          setVersion.onsuccess = function () {
            createObjectStore(db);
            getImageFile();
          };
        }
        else {
          getImageFile();
        }
      }
      else {
        getImageFile();
      }
    }
    return request
  }

}